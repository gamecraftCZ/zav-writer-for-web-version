from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.remote import webelement
from time import sleep
import random


username = ""
password = ""


def main():
    ## INITIATE ##
    driver = webdriver.Chrome(
        executable_path="./chromedriver.exe")

    driver.get("https://student.zav.cz")
    sleep(3)

    ## LOGIN ##
    loginField = driver.find_element_by_xpath(
        "/html/body/div/div/div/div[2]/div[1]/form/div[1]/div/input")
    loginField.click()
    loginField.clear()
    loginField.send_keys(username)

    passwordField = driver.find_element_by_xpath(
        "/html/body/div/div/div/div[2]/div[1]/form/div[2]/div/input")
    passwordField.click()
    passwordField.clear()
    passwordField.send_keys(password)

    loginButton = driver.find_element_by_xpath(
        "/html/body/div/div/div/div[2]/div[1]/form/input")
    loginButton.click()

    sleep(3)

    # OPEN WRITING
    openElement = driver.find_element_by_xpath(
        "/html/body/div/div[2]/form/div[2]/div[1]/div/div/div/a")
    openElement.click()
    sleep(3)

    ## WRITE ##

    textElement = driver.find_element_by_xpath(
        "/html/body/div/div[3]/div/div[2]/view2/div[1]/scrollable-text/div/div/div[1]")
    text = textElement.text

    inp = driver.find_element_by_xpath(
        "/html/body/div/div[3]/div/div[2]/view2/div[2]/text-input/div/div[1]")
    inp.click()

    print("--- Started writing ---")

    try:
        for l in text:
            sleep((200 + random.random() * 100) / 1000)
            print(l, end="")
            inp.send_keys(l)
    except Exception as e:
        print()
        print("EXP: ")
        print(e)

    print()
    print("--- Stopped writing ---")

    sleep(10)
    nextButton = driver.find_element_by_xpath(
        "/html/body/div/div[3]/div/div[2]/view2/div[1]/lesson-panel/div/table/tbody/tr/td[2]/button[1]")
    nextButton.click()
    sleep(3)

    return driver


if __name__ == "__main__":
    main()
